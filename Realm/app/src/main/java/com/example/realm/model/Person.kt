package com.example.realm.model

import io.realm.kotlin.types.RealmObject

class Person() : RealmObject {
    private var _firstName : String = ""
    private var _lastName: String = ""
    private var _age: Int = -1

    constructor(fisrtName: String, lastName: String, age: Int) : this(){
        this._firstName = fisrtName
        this._lastName = lastName
        this._age = age
    }

    override fun toString(): String {
        var msg = "first name is {$_firstName} and last name is {$_lastName}, age = {$_age}"
        return msg
    }
}