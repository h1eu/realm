package com.example.realm.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.realm.databinding.LayoutItemBinding
import com.example.realm.model.Person
import io.realm.kotlin.query.RealmResults

class PersonAdapter(private val list : RealmResults<Person>) :
    RecyclerView.Adapter<PersonAdapter.PersonViewHolder>() {
    inner class PersonViewHolder(binding : LayoutItemBinding) : RecyclerView.ViewHolder(binding.root){
        val bd : LayoutItemBinding
        init {
            bd = binding
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val inflate = LayoutInflater.from(parent.context)
        val binding = LayoutItemBinding.inflate(inflate,parent,false)
        return PersonViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        val person = list[position]
        holder.bd.tv.text = person.toString()
    }

    override fun getItemCount(): Int {
        return list.size
    }
}