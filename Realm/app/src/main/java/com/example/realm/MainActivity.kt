package com.example.realm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.realm.adapter.PersonAdapter
import com.example.realm.databinding.ActivityMainBinding
import com.example.realm.model.Person
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import io.realm.kotlin.ext.query
import io.realm.kotlin.query.RealmResults
import io.realm.kotlin.query.Sort

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private lateinit var config : RealmConfiguration
    private lateinit var realm : Realm
    private lateinit var personList : RealmResults<Person>
    private var list : ArrayList<String> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        connectRealm()
        //initData()
       // query()
        sortQuery()
        addData()
        setupRv()
    }


    private fun connectRealm() {
        config = RealmConfiguration.create(schema = setOf(Person::class))
        realm = Realm.open(config)
    }

    private fun initData(){
        realm.writeBlocking {
            this.copyToRealm(Person("le","hieu",22))
            this.copyToRealm(Person("hoang","pham",34))
            this.copyToRealm(Person("le","hung",12))
            this.copyToRealm(Person("tam","nguyen",24))
            this.copyToRealm(Person("thanh","le",25))
            this.copyToRealm(Person("pham","giang",13))
        }
    }

    private fun findQuery(){
        personList = realm.query<Person>("_age > 20 AND _firstName = 'le'").find()
    }

    private fun sortQuery(){
        personList = realm.query<Person>("_age > 22")
            .sort("_age",Sort.DESCENDING)
            .distinct("_firstName")
            .limit(5)
            .find()
    }

    private fun query(){
        personList = realm.query<Person>().find()
    }

    private fun addData(){
        for(i in 0 until personList.size){
            list.add(personList[i].toString())
        }
    }

    private fun setupRv(){
        var layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        var adapter = PersonAdapter(personList)
        binding.rv.layoutManager = layoutManager
        binding.rv.adapter = adapter
    }
}